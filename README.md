CONTENTS OF THIS FILE
---------------------

* Introduction
* Requirements
* Installation
* Maintainers


INTRODUCTION
------------

Provides integration with the Compressor.js JavaScript library
(https://github.com/fengyuanchen/compressorjs) compressing images prior to them
being uploaded to the server.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/compressorjs

 * To submit bug reports and feature suggestions, or track changes:
   https://www.drupal.org/project/issues/compressorjs


REQUIREMENTS
------------

This module requires the following modules:

* Image (https://www.drupal.org/project/drupal)


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit:
https://www.drupal.org/docs/user_guide/en/extend-module-install.html for further
information.


MAINTAINERS
-----------

This project is sponsored and maintained by:

  * ANTOINE SOLUTIONS
    Specialized in consulting and architecting robust web applications powered
    by Drupal.
