/**
 * @file
 *
 * Compresses images in the browser.
 */
(function (Drupal, drupalSettings, $) {
  drupalSettings.compressorjs = {
    maxWidth: 1920,
    maxHeight: 1920,
    quality: 0.6,
  };

  Drupal.compressorjs = {
    compressorjs: function compressorjs(event) {
      // Get the file from the input element.
      const file = event.target.files[0];

      if (!file) {
        return;
      }

      new Compressor(file, {
        maxWidth: drupalSettings.compressorjs.maxWidth,
        maxHeight: drupalSettings.compressorjs.maxHeight,
        quality: drupalSettings.compressorjs.quality,
        success(result) {
          try {
            // Convert the result to a file object.
            const dataTransfer = new DataTransfer();
            dataTransfer.items.add(new File([result], result.name));
            // Replace the input's files.
            event.target.files = dataTransfer.files;
          }
          catch (err) {
            // If the DataTransfer class is not supported, we cannot update
            // event.target.files.
          }
          // Call the default autoFileUpload handler.
          Drupal.file.triggerUploadButton(event);
        },
        error(err) {
          console.log(err.message);
        },
      });
    }
  };

  const fileAutoUpload = Drupal.behaviors.fileAutoUpload;
  Drupal.behaviors.fileAutoUpload = {
    attach: function attach(context) {
      $(context).find('input[type="file"][accept!="image/*"]').once('auto-file-upload').on('change.autoFileUpload', Drupal.file.triggerUploadButton);
      $(context).find('input[type="file"][accept="image/*"]').once('compressorjs').on('change.compressorjs', Drupal.compressorjs.compressorjs);
    },
    detach: function detach(context, settings, trigger) {
      if (trigger === 'unload') {
        $(context).find('input[type="file"][accept!="image/*"]').removeOnce('auto-file-upload').off('.autoFileUpload');
        $(context).find('input[type="file"][accept="image/*"]').removeOnce('compressorjs').off('.compressorjs');
      }
    }
  }

})(Drupal, drupalSettings, jQuery);

